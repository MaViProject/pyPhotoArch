#!/usr/bin/env python3

'''
Class to deal with files and folders.
It implements some current use function.
It implements a logger.

@Author: Mario Vitale
'''

import os
import datetime as dt
import logging
import json

class FileUtils:

    def __init__(self,logFolder=False,logLevel="INFO"):
        # initialize a logger
        logname = str(self.dtPrefix())+"FileUtils.log"
        if not os.path.exists(logFolder):
            os.mkdir(logFolder)
        logname = os.path.join(logFolder,logname)
        self.logger = logging.getLogger(logname)
        hdlr = logging.FileHandler(logname)
        formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
        hdlr.setFormatter(formatter)
        self.logger.addHandler(hdlr)
        self.logger.propagate = False
        if logLevel == "DEBUG":
            self.logger.setLevel(logging.DEBUG)
        elif logLevel == "ERROR":
            self.logger.setLevel(logging.ERROR)
        else:
            self.logger.setLevel(logging.INFO)

    def setLogLevel(self,logLevel):
        self.logger.setLevel(logLevel)

    def dtPrefix(self,format="%d%m%Y",separator="_"):
        today = dt.date.today()
        d1 = today.strftime(format)
        return (str(d1)+separator)


    # create a dir
    def createDir(self,path,dirmode=0o666):       
        try:
            if os.path.exists(path):
                self.logger.debug("createDir - The folder "+str(path)+" already exists. Nothing to create.")
                return True
            else:
                os.mkdir(path=path,mode=dirmode)
                self.logger.debug("createDir - The folder "+str(path)+" has been succesfully created.")
            return True
        except OSError as oserr:
            self.logger.error("createDir - An error occurred while creating folder "+str(oserr))
            return False

    # gets a json toRead (full path to file) and returns a parsed object
    def jsonReader(self, toRead):
        if os.path.exists(toRead):
            try:
                with open(toRead, 'r') as content:
                    data=content.read()
            except Exception as e:
                self.logger.error("jsonReader - An error occurred while reading the "+ str(toRead)+"file "+str(e))
                return False
            # parse
            obj = json.loads(data)
            return obj
        else:
            self.logger.error("jsonReader - The json file "+str(toRead)+" has not been found.")
            return False