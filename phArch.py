#!/usr/bin/env python3

'''
Class to implement the peculiar app functions.
It is the core of the application.

@Author: Mario Vitale
'''

import logging
import os
from PIL import Image, ExifTags
import reverse_geocoder as rg 
import fileUtils as fu
import timeUtils as tu

class PhArch:
    # initialize the application and the external useful classes
    def __init__(self,logFolder='logs',logLevel="INFO"):
        
        # instance variables
        self.cfg = 'config.json'
        self.dtFormatCatalogue = 'dateFrmtDef.json'
        self.dirmode = 0o666

        # initialize a xUtils instance
        self.fu = fu.FileUtils(logFolder=logFolder,logLevel=logLevel)
        self.tu = tu.TimeUtils(logFolder=logFolder,logLevel=logLevel)
        
        # initialize a logger
        logname = str(self.tu.dtPrefix())+"PhArch.log"
        if not os.path.exists(logFolder):
            os.mkdir(logFolder)
        logname = os.path.join(logFolder,logname)
        self.logger = logging.getLogger(logname)
        hdlr = logging.FileHandler(logname)
        formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
        hdlr.setFormatter(formatter)
        self.logger.addHandler(hdlr)
        self.logger.propagate = False
        if logLevel == "DEBUG":
            self.logger.setLevel(logging.DEBUG)
        elif logLevel == "ERROR":
            self.logger.setLevel(logging.ERROR)
        else:
            self.logger.setLevel(logging.INFO)
        
    # changes the log level for this class and for the initializate helper class instanciated
    def setLogLevel(self,logLevel):
        self.logger.setLevel(logLevel)
        self.fu.setLogLevel(logLevel)
        self.tu.setLogLevel(logLevel)

        
        
    #### App-related methods

    # Read metadata for a single image
    def readTags(self,imgpath):
        date=""
        camera=""
        place=""
        try:
            img = Image.open(imgpath)
            try: 
                exif = { ExifTags.TAGS[k]: v for k, v in img._getexif().items() if k in ExifTags.TAGS }
            except Exception as exifEx:
                self.logger.error("Impossible to get tags from the image: "+str(imgpath)+" due to "+str(exifEx))
                return False
            #logger.debug("Metadata of "+str(imgpath)+": "+str(exif))
            if(exif['DateTimeOriginal']):
                date=exif['DateTimeOriginal'][0:10].replace(":","")
                self.logger.debug('The image '+str(imgpath)+' has been shooted in date: '+str(date))
            else:
                self.logger.debug("The image "+str(imgpath)+" has no original date-time.")
            if(exif['Model']):
                camera=exif['Model']
                self.logger.debug('The image '+str(imgpath)+' has been shooted using a: '+str(camera)+' by '+str(exif['Make']))
            else:
                self.logger.debug("The image "+str(imgpath)+" has no data on used camera.")
            
            # only if there is GPSInfo
            if ('GPSInfo' in exif and len(exif['GPSInfo'])>1 and exif['GPSInfo'][2] and exif['GPSInfo'][4]):
                print("has GPS")    
                latitude = self.convert_to_degrees(exif['GPSInfo'][2])
                longitude = self.convert_to_degrees(exif['GPSInfo'][4])
                coordinates = (latitude, longitude)
                # only 1 location per photo
                location = rg.search(coordinates)[0]
                place = location['name']
            else:
                self.logger.debug("The image "+str(imgpath)+" has no GPS coordinates.")

            return {'date':date,'camera':camera,'place':place}

        except Exception as e:
            self.logger.error("An error occurred while reading the image tags in "+str(imgpath)+" : "+str(e))
            return False


    # Converting value to degrees
    def convert_to_degrees(self,value):
        #print("conversion funct called ")
        try:
            deg = value[0]
            minute = value[1]
            sec = value[2]
            coords = deg + (minute / 60.0) + (sec / 3600.0)
            #print("coords "+str(coords))
            return coords

        except Exception as e:
            self.logger.error("An error occurred while converting the coordinate in degrees: "+str(e))
            return False
    
    # gets the date format like YYYYMMDD and returns its dtformat to use in the datetime functions
    def getDateFormatFromCatalogue(self,dateFormat):
        if not os.path.exists(self.dtFormatCatalogue):
            self.logger.error("getDateFormatFromCatalogue - The date-format catalogue has not been found.")
            return False
        # read the date format catalogue
        catalogue = self.fu.jsonReader(self.dtFormatCatalogue) 
        for frmt in catalogue:
            if frmt['format']==dateFormat:
                return frmt['dtformat']
        # if reach this point means dateFormat doesn't match
        self.logger.error("getDateFormatFromCatalogue - dateFormat "+str(dateFormat)+" does not match with any entry of the catalogue.")
        return False
        

    def createFolderByTag(self,cfg,phfolder,tags):
        self.logger.debug("createFolderByTag - Create folder for these tags: "+str(tags))
        if cfg['destFolder']=="":
            baseFolder = phfolder
        else:
            baseFolder = cfg['destFolder']
        self.logger.debug("createFolderByTag - The baseFolder for the structure is "+str(baseFolder))

        if cfg['withTags']['hierarchical']:
            # set folder creation status
            dfCreated = False
            locCreated = False
            camCreated = False

            self.logger.debug("createFolderByTag - Creating a hierarchical structure...")
            if cfg['withTags']['withDate'] and len(tags['date']):
                # with tags original format is always %Y%m%d so the default
                destFormat = self.getDateFormatFromCatalogue(cfg['dateFormat'])
                date = self.tu.convertDt(date=tags['date'],destFormat=destFormat)
                # error in date format: image ignored
                if not date:
                    return False
                
                # create a date folder
                dateFolder = os.path.join(baseFolder,date)
                dfCreated = self.fu.createDir(dateFolder,self.dirmode)
                if not dfCreated:
                    self.logger.error("createFolderByTag - Impossible to create the date folder for the structure. Application quits.")
                    return False
                else: 
                    self.logger.debug("createFolderByTag - Date folder has been created now checks for other tags.")
            
            # set the location under the date or on the first level
            if cfg['withTags']['withLocation'] and len(tags['place'])>0:
                if dfCreated:
                    locFolder = os.path.join(dateFolder,tags['place'])
                else:
                    locFolder = os.path.join(baseFolder,tags['place'])
                locCreated = self.fu.createDir(locFolder,self.dirmode)
                if not locCreated:
                    self.logger.error("createFolderByTag - Impossible to create the location folder for the structure.")
                    return False
                else: 
                    self.logger.debug("createFolderByTag - Location folder has been created now checks for camera.")


            # set the camera under the location or under the date or at first level 
            if cfg['withTags']['withCamera'] and len(tags['camera'])>0:
                if locCreated:
                    camFolder = os.path.join(locFolder,tags['camera'])
                elif dfCreated and not locCreated:
                    camFolder = os.path.join(dateFolder,tags['camera'])
                elif not dfCreated and not locCreated:
                    camFolder = os.path.join(baseFolder,tags['camera'])
                # create the folder
                camCreated = self.fu.createDir(camFolder,self.dirmode)
                if not camCreated:
                    self.logger.error("createFolderByTag - Impossible to create the camera folder for the structure. Application quits.")
                    return False
                else: 
                    self.logger.debug("createFolderByTag - Camera folder has been created. Lower level.")
                
            # set the return dest folder basing on the creation hierarchy
            if camCreated:
                return camFolder
            elif locCreated:
                return locFolder
            elif dfCreated:
                return dateFolder

        # if not hierarchical
        else:        
            flatName = ''
            if cfg['withDate'] and len(tags['date'])>0:
                # with tags original format is always %Y%m%d so the default
                destFormat = self.getDateFormatFromCatalogue(cfg['dateFormat'])
                date = self.tu.convertDt(date=tags['date'],destFormat=destFormat)
                # error in date format: image ignored
                if not date:
                    return False
                flatName = date
            if cfg['withLocation'] and len(tags['place'])>0:
                flatName = flatName + cfg['flatSeparator'] + tags['place']
            if cfg['withCamera'] and len(tags['camera'])>0:
                flatName = flatName + cfg['flatSeparator'] + tags['camera']
            destFolder = os.path.join(baseFolder,flatName)
            destCreated = self.fu.createDir(destFolder,self.dirmode)
            if destCreated:
                self.logger.debug("createFolderByTag - Destination folder "+str(destFolder)+" has been succesfully created")
                return destFolder
            else: 
                self.logger.error("createFolderByTag - Impossible to create the destination folder "+ str(destFolder))
                return False


    # creates a folder in the base folder basing on the date in the name if it exists
    # converts date in destination format
    def createFolderByName(self,cfg,phfolder,imgName):
        self.logger.debug("createFolderByName - Creating folder for this image: "+str(imgName))
        # set the parent folder for the new folder we're going to write
        if cfg['destFolder']=="":
            baseFolder = phfolder
        else:
            baseFolder = cfg['destFolder']
        self.logger.debug("createFolderByName - The baseFolder for the structure is "+str(baseFolder))

        # get the date from the name according to its format.
        frmtLength = 0
        found = False
        dtFormatOrig = False
        dtCat = self.fu.jsonReader(self.dtFormatCatalogue) 
        for frmt in dtCat:
            if frmt['format'] == cfg['noTags']['dateFormatName']:
                frmtLength = frmt['length']
                dtFormatOrig = frmt['dtformat']
                found = True
        if not found:
            self.logger.error("createFolderByName - The format "+str(cfg['dateFormatName'])+" specified for the name has not been found in the json date format catalogue")
            return False
        if not dtFormatOrig:
            self.logger.error("createFolderByName - No dtformat value has been found for "+str(cfg['dateFormatName'])+" in the json date format catalogue")
            return False
        #print(frmtLength)
        # get the substring starting from after prefix and length as frmtLength
        start=imgName.index(cfg['noTags']['datePrefix'])+len(cfg['noTags']['datePrefix'])
        end = start+frmtLength
        date = imgName[start:end]
        self.logger.debug("createFolderByName - The date found parsing the file name "+str(imgName)+" is: "+str(date))
        print('date from name: '+str(date))
        destFormat = self.getDateFormatFromCatalogue(cfg['dateFormat'])
        date = self.tu.convertDt(date=date,destFormat=destFormat,origFormat=dtFormatOrig)
        print('date from name after set date conversion: '+str(date))
        # error in date format: image ignored
        if not date:
            return False
        destFolder = os.path.join(baseFolder,date)
        destCreated = self.fu.createDir(destFolder,self.dirmode)
        if destCreated:
            self.logger.debug("createFolderByName - Destination folder "+str(destFolder)+" has been succesfully created")
            return destFolder
        else: 
            self.logger.error("createFolderByName - Impossible to create the destination folder "+ str(destFolder))
            return False


    # verify the data in the json are as expected (hardcoded expectations)
    def verifyCfgData(self,obj):
        allowedDateFormats=[]
        dtCat = self.fu.jsonReader(self.dtFormatCatalogue) 
        for frmt in dtCat:
            allowedDateFormats.append(frmt['format'])

        try:
            # check loglevel
            if isinstance(obj['logLevel'],str):
                if obj['logLevel'] in ('DEBUG','INFO','ERROR'):
                    pass
                else: 
                    self.logger.error("verifyCfgData - The logLevel value is not allowed: "+str(obj['logLevel'])+". Allowed values are only: DEBUG, INFO or ERROR.")
                    return False
            else:
                self.logger.error("verifyCfgData - The logLevel value is not a string but a "+str(type(obj['logLevel'])))
                return False
            
            # check dateFormat
            if isinstance(obj['dateFormat'],str):
                if obj['dateFormat'] in (allowedDateFormats):
                    pass
                else: 
                    self.logger.error("verifyCfgData - The dateFormat value is not allowed: "+str(obj['dateFormat'])+". Allowed values are in the documentation.")
                    return False
            else:
                self.logger.error("verifyCfgData - The dateFormat value is not a string but a "+str(type(obj['dateFormat'])))
                return False
            
            # check destinationFolder
            if isinstance(obj['destFolder'],str):
                pass
            else:
                self.logger.error("verifyCfgData - The destFolder value is not a string but a "+str(type(obj['destFolder'])))
                return False
            
            # check imgFormats
            if isinstance(obj['imgFormats'],list):
                pass
            else:
                self.logger.error("verifyCfgData - The imgFormats value is not a string but a "+str(type(obj['imgFormats'])))
                return False
            
            # check dateFormatName
            if isinstance(obj['noTags']['dateFormatName'],str):
                if obj['noTags']['dateFormatName'] in (allowedDateFormats):
                    pass
                else: 
                    self.logger.error("verifyCfgData - The dateFormatName value is not allowed: "+str(obj['noTags']['dateFormatName'])+". Allowed values are in the documentation.")
                    return False
            else:
                self.logger.error("verifyCfgData - The dateFormatName value is not a string but a "+str(type(obj['noTags']['dateFormatName'])))
                return False
            
            # check datePrefix
            if isinstance(obj['noTags']['datePrefix'],str):
                pass
            else:
                self.logger.error("verifyCfgData - The datePrefix value is not a string but a "+str(type(obj['noTags']['datePrefix'])))
                return False
            
            # check hierarchical
            if isinstance(obj['withTags']['hierarchical'],bool):
                pass
            else:
                self.logger.error("verifyCfgData - The hierarchical value is not a boolean but a "+str(type(obj['withTags']['hierarchical'])))
                return False
            
            # check withDate
            if isinstance(obj['withTags']['withDate'],bool):
                pass
            else:
                self.logger.error("verifyCfgData - The withDate value is not a boolean but a "+str(type(obj['withTags']['withDate'])))
                return False

            # check withLocation
            if isinstance(obj['withTags']['withLocation'],bool):
                pass
            else:
                self.logger.error("verifyCfgData - The withLocation value is not a boolean but a "+str(type(obj['withTags']['withLocation'])))
                return False
            
            # check withCamera
            if isinstance(obj['withTags']['withCamera'],bool):
                pass
            else:
                self.logger.error("verifyCfgData - The withCamera value is not a boolean but a "+str(type(obj['withTags']['withCamera'])))
                return False

            # check flatSeparator
            if isinstance(obj['withTags']['flatSeparator'],str):
                pass
            else:
                self.logger.error("verifyCfgData - The flatSeparator value is not a string but a "+str(type(obj['withTags']['flatSeparator'])))
                return False

            self.logger.debug("verifyCfgData - All the json fields have the expected values.")
            return True
        
        # If there's an error while reading the object then the configurator is corrupted
        except Exception as e:
            self.logger.error("verifyCfgData - An error occurred while reading the json: "+str(e))
            return False

    # Main method to call receiving the path
    def archive(self, phPath):
        # check if the photo file path exists
        if not os.path.exists(phPath):
            self.logger.error("archive - The specified photo folder "+str(phPath)+ " does not exist.")
            return False

        # get the config file and checks it
        if not os.path.exists(self.cfg):
            self.logger.error("archive - The config file "+str(self.cfg)+ " does not exist in the application root.")
            return False
        
        cfg = self.fu.jsonReader(self.cfg)
        if not self.verifyCfgData(cfg):
            self.logger.error("archive - The config file is not properly written.")
            return False

        # set the required logLevel
        self.setLogLevel(cfg['logLevel'])

        # move each photo in the path to the structure.
        # In case of errors pass to next file.
        for img in os.listdir(phPath):
            # try read the tags
            image = os.path.join(phPath,img)
            # if is a file (avoid folders) and is an allowed format order
            allwdImgFormats = tuple(cfg['imgFormats'])
            if os.path.isfile(image) and image.endswith(allwdImgFormats):
                data = self.readTags(image)
                # if no tag try to read the name
                if(not data):
                    self.logger.info("archive - The image "+str(image)+" has no tags, try with the name.")
                    structCreated = self.createFolderByName(cfg=cfg,phfolder=phPath,imgName=img)
                    #pass
                # if there are tags then order according to the cfg settings
                else:
                    self.logger.info("archive - The image "+str(image)+" has tags, try to use them to create a folder.")
                    structCreated = self.createFolderByTag(cfg=cfg,phfolder=phPath,tags=data)
                
                #in both cases the photo must be moved in the returned folder if possible
                if not structCreated:
                    self.logger.error("archive - Impossible to create the structure for "+str(img)+ " pass to next one")
                    pass
                else:
                    self.logger.info("archive - Structure created, now move the photo to "+str(structCreated))
                    try:
                        destImg = os.path.join(structCreated,img)
                        os.replace(image,destImg)
                    except OSError as oserr:
                        self.logger.error("archive - An error occurred while moving the file "+str(img)+" due to: "+str(oserr)+" move to next one")
                        pass
        
            # if is a directory or another file format then ignore            
            else:
                self.logger.debug(str(image)+" Has been excluded as it is a directory or a non compliant format.")
                pass
        
        return True

        

        
