# Application

Python utility application to order photos.
The application tries to read the photos tags. 
- If they exist then creates folders where to move them according to some role. There can be a flat or hierarchical structure 
- If they don't exist then it tries to read the file name looking for a date to use to group photos. In that case is only possible to group the images by date.

In case of tags the folder structure can be hierarchical or flat and include
- date: when the photo has been shooted: ***DateTimeOriginal*** tag
- place: where the photo has been shooted resolving the GPS coordinates (if available)
- camera: the camera model used to shoot the photo: ***Model*** tag

If some of the tag is null then the level is skipped.

All the applicable options are in the JSON config file ***config.json*** placed in the root application folder. If the *config.json* is removed the application quits with error.

> **N.B.:** The python3 dependencies are in a ***requirements.txt*** file.


# JSON config file
The config file placed in the root of the application is named ***config.json*** and is used to setup: 
- **logLevel**: is the logging level, how detailed is the operation report file named ***date_phArchiver*** where the *date* part is replaced by the operations date.
- **dateFormat**: is the date format used to create the destination folder
- **destFolder**: where the new structure will be created. If empty is the photoFolder itself.
- **imgFormats**: array of allowed image extensions. (not active in this version)
- **noTag**: is the wrapper for the option used in case the photo has no tags
    - **dateFormatName**: is the format of the date written in the file name
    - **datePrefix**: is the text written in file name before the date. In case it is specified the date will start immediately after it, otherwise the date is expected at the beginning of the filename.
- **withTags**: is the wrapper for the option used in case the photo has tags 
    - **hierarchical**: is the destination folder structure. It accepts:
        - *True*: the structure will be made by nested folder. The folder names will be taken by tag values. The top folder is the date, then the location then the camera.
        - *False*. the structure is flat, only 1 folder named using the tags separated by *flatSeparator*.
    - **flatSeparator**: symbol to separate the tags in the folder name in case the structure is not hierarchical (False).
    - **withDate**: accepts *True/False* and define if use or not the date tag in the structure
    - **withLocation**: accepts *True/False* and define if use or not the location tag in the structure
    - **withCamera**: accepts *True/False* and define if use or not the camera tag in the structure

If the matching is impossible then the photo is left where it is.



## Hierarchy
The hierarchical structure is fixed. If all the tags are enabled and populated it is structured as it follows
```tree
|-- date
|    |
|    |-- location
|    |    |
|    |    |-- camera
```
If one of the tag is missing or disabled the structure is scalated. As example, if location is missing or disabled
```tree
|-- date
|    |
|    |-- camera
```


## Log
A log file named *[date]_phArchiver* (where date is replaced by the file date) is created containing the backup results. The default backup level is DEBUG but it can be configured by XML, possible values are:
- **DEBUG**: detailed logging, each operation is written in the log file
- **ERROR**: only the errors are reported in the log file
- **INFO**: only the errors and the main info are reported in the log file

## Date
The allowed date formats are:
1. YYYYMMDD: e.g. 20201213
2. DDMMYYYY: e.g. 13202020
3. YYYYMMMMDD: e.g. 2020Dec13 
4. DDMMMMYYYY: e.g. 13Dec2020 



## JSON example

```json
{
  "logLevel":"DEBUG",
  "dateFormat":"DDMMYYYY",
  "destFolder":"",
  "imgFormats":["png","jpg","jpeg","tiff"],
  "noTags": {
    "dateFormatName":"YYYYMMDD",
    "datePrefix":"IMG-"
  },
  "withTags":{
    "hierarchical": true,
    "withDate":true,
    "withLocation":true,
    "withCamera":true,
    "flatSeparator":"_"
  }
}
```


# Usage 
First the dependencies must be installed
```shell
$ pip3 install --no-cache-dir -r requirements.txt
``` 
Then it can be launched from the folder where the *phArch.py* lies
```shell
$ python3 pharchiver.py -f path/to/photo/folder -c config.json
```


# TO-DO
This project is an implementation for educational purpose. 
Further steps are:
- Graphical interface to set the config instead of using json.
- Testing


