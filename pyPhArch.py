#!/usr/bin/env python3

'''
Interface. Is the entry points.
Deals with inbound parameters and uses the classes to execute the application

@Author: Mario Vitale
'''


import argparse
import phArch as pa
import os
import sys

# get arguments from command line
def get_args():

    parser = argparse.ArgumentParser()
    parser.add_argument("-f", "--folder", dest="phfolder", help="path to the folder containing the photo to order (full path)")
    options = parser.parse_args()

    if not options.phfolder:
        return parser.error("No image folder has been specified")
    return options



##### Entry point
if __name__ == "__main__":

    # get args passed by shell
    opts = get_args()
    # deals with the application class
    parch = pa.PhArch()
    archived = parch.archive(opts.phfolder)
    
    # Prompts the results and quit
    if not archived:
        sys.exit("[-] Application quits due to an error, check the logs.")

    else: 
        sys.exit("[+] Your photo have been archived in folders. Check the logs for some issue.")


# END
    
