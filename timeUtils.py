#!/usr/bin/env python3

'''
Class to deal with time functions.
It implements some current use function.
It implements a logger.

@Author: Mario Vitale
'''
import datetime as dt
import logging
import os


class TimeUtils:

    def __init__(self,logFolder='logs',logLevel="INFO"):
        # initialize a logger
        logname = str(self.dtPrefix())+"TimeUtils.log"
        if not os.path.exists(logFolder):
            os.mkdir(logFolder)
        logname = os.path.join(logFolder,logname)
        self.logger = logging.getLogger(logname)
        hdlr = logging.FileHandler(logname)
        formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
        hdlr.setFormatter(formatter)
        self.logger.addHandler(hdlr)
        self.logger.propagate = False
        if logLevel == "DEBUG":
            self.logger.setLevel(logging.DEBUG)
        elif logLevel == "ERROR":
            self.logger.setLevel(logging.ERROR)
        else:
            self.logger.setLevel(logging.INFO)
    
    def setLogLevel(self,logLevel):
        self.logger.setLevel(logLevel)

    # create a date prefix with today's date
    def dtPrefix(self,format="%d%m%Y",separator="_"):
        today = dt.date.today()
        d1 = today.strftime(format)
        return (str(d1)+separator)

    # convert date format.  
    def convertDt(self,date,destFormat,origFormat='%Y%m%d'):
        if type(date)==str:
            # convert the original to datetime in origFormat
            try:
                dateDt = (dt.datetime.strptime(date,origFormat))
                self.logger.debug("convertDt - Date in string as been converted in datetime.")
            except Exception as e:
                self.logger.error("convertDt - The conversion of "+str(date)+" in format "+str(origFormat)+" from string to datetime failed due to: "+str(e)) 
                return False
        # convert the datetime from one format to the other
        try:
            dateDt = dateDt.strftime(destFormat)
            return dateDt

        except Exception as e:
            self.logger.error("convertDt - Impossible to convert the date format to "+str(destFormat)+" due to "+str(e))
            return False

        
